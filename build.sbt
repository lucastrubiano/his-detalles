name := "InsertHisDetallesHash"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.8"

idePackagePrefix := Some("org.novakorp.com")

mainClass in (packageBin) := Some("org.novakorp.com.entry")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-yarn" % sparkVersion,
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "org.scalatest" %% "scalatest" % "3.0.1" % Test,
  "com.databricks" %% "spark-csv" % "1.2.0"
)

fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
// Show runtime of tests
testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oD")