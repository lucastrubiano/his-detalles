package org.novakorp.com

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, lit}

import java.io.{FileOutputStream, PrintStream}

object entry extends SparkSessionWrapper {

  def main(args: Array[String]): Unit = {

    val environment: String  = args(0)
    val country: String      = args(1)
    val tableName1: String   = args(2) //his_detalles
    val tableName2: String   = args(3) //provider_import_log
    val processDate: String  = args(4)
    val idCierre: String     = args(5)
    val yearMonth: String    = args(6)
    val filePath: String     = args(7)
    val separator: String    = args(8)

    val outputTable1 = s"${environment}_${country}_raw.$tableName1"
    val outputTable2 = s"${environment}_${country}_cur.$tableName2"
    val outputFilePath1 = s"hdfs://arbd1cl-ns/user/admin/${country}/01-raw/$tableName1"
    val outputFilePath2 = s"hdfs://arbd1cl-ns/user/admin/${country}/02-cur/$tableName2"


    spark.sparkContext.setLogLevel("ERROR")

    try {
      // READ DATAFRAME
      val df = transforms.readDataFrame(outputTable1, filePath, separator)
      println("STEP 1: READ DATAFRAME - OK")

      // HASH CONTENT
      val hashesDf: DataFrame = transforms.hashContent(df)
      hashesDf.cache()
      println("STEP 2: HASH CONTENT - OK")

      // JOIN HIS_DETALLES WITH HASHES
      import spark.implicits._
      val dfWithHash: DataFrame = df.as('df1)
        .join( hashesDf.as('df2) )
        .where($"df1.w_origin_entity_pk" === $"df2.w_origin_entity_pk")
        .select($"df1.*",$"df2.hash")


      // INSERT INTO HIS_DETALLES
      val hisDetallesDf = dfWithHash.withColumn("fecha_proceso", lit(processDate))
        .withColumn("id_cierre", lit(idCierre))
        .withColumn("provider", col("w_origin_entity_pk"))
        .withColumn("year_month", lit(yearMonth))
        .withColumnRenamed("hash", "hashvalue")
      println("hisDetallesDf - OK")
      hisDetallesDf.printSchema()


      transforms.insertDataFrame(hisDetallesDf, outputTable1)
      println("STEP 3: INSERT HIS_DETALLES - OK")
      /**
       // CREATE HIS_DETALLES WITH SPARK
        val partitionsHisDetalles = Array("year_month")
        transforms.createHisDetalles(hisDetallesDf, partitionsHisDetalles, outputTable1, outputFilePath1)
       */

      // INSERT INTO PROVIDER_IMPORT_LOG
      val provImporLogDf = hashesDf.withColumnRenamed("hash", "hashvalue")
        .withColumn("process_date", lit(processDate))
        .withColumn("id_cierre", lit(idCierre))
        .withColumn("year_month", lit(yearMonth))
        .withColumnRenamed("w_origin_entity_pk", "provider")
      println("provImporLogDf - OK")


      transforms.insertDataFrame(provImporLogDf, outputTable2)
      println("STEP 4: INSERT PROVIDER_IMPORT_LOG - OK")

      /** CREATE PROVIDER_IMPORT_LOG WITH SPARK
       * val partitionsProvImporLog = Seq("id_cierre","year_month")
       * transforms.createProvImporLog(provImporLogDf, partitionsProvImporLog, outputTable2, outputFilePath2)
       */

    }
    catch {
      case e: Exception =>
        throw new RuntimeException(e.getStackTrace.mkString("\n"))
    }
    finally{
      spark.sparkContext.stop()
    }

  }

}
