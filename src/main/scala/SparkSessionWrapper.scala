package org.novakorp.com

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf

trait SparkSessionWrapper extends Serializable {

  lazy val conf: SparkConf = new SparkConf()
    .setAppName("Insert His_Detalles")
    .setMaster("yarn")
    .set("spark.executor.memory", "12g")
    .set("spark.driver.memory", "12g")
    .set("spark.executor.instances", "4")
    .set("hive.exec.dynamic.partition", "true")
    .set("hive.exec.dynamic.partition.mode", "nonstrict")
    .set("spark.sql.sources.partitionOverwriteMode","dynamic")

  lazy val spark: SparkSession = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()
}
