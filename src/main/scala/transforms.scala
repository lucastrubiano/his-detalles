package org.novakorp.com

import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.functions.{col, hash, md5}
import org.apache.spark.sql.types.{IntegerType, ShortType, StringType, StructType}

object transforms extends SparkSessionWrapper {

  def readDataFrame(outputTable: String, filePath: String, separator: String): DataFrame = {

    val schema: StructType = spark.sql(s"select * from ${outputTable} limit 1")
      .drop("hashvalue","fecha_proceso","id_cierre","provider","year_month")
      .schema

    val newDf = spark.read.format("csv")
      .options(Map("header"->"false","delimiter"->separator,"nullValue"-> "null"))
      .schema(schema)
      .load(filePath)

    newDf
  }

  def hashContent(df: DataFrame): DataFrame = {

    val numericHashDf = df
      .withColumn("hash", hash(df.columns.map(col):_*))
      .groupBy(col("w_origin_entity_pk"))
      .avg("hash")

    val md5HashDf = numericHashDf.select(
      col("w_origin_entity_pk"),
      md5(col("avg(hash)").cast(StringType)).alias("hash") )

    md5HashDf
  }

  def createHisDetalles(df: DataFrame, partitions: Seq[String], outputTable: String, outputFilePath: String): Unit = {
    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    //val dfToInsert = df.select(col()) // Aca pueden ir las columnas

    df.write
      .mode("overwrite")
      .partitionBy(partitions: _*)
      .option("path", outputFilePath)
      .saveAsTable(outputTable)
  }

  def createProvImporLog(df: DataFrame, partitions: Seq[String], outputTable: String, outputFilePath: String): Unit = {
    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    //val dfToInsert = df.select(col()) // Aca pueden ir las columnas

    import spark.implicits._
    val newSchemaDf: DataFrame = df.select(
      $"hashvalue",
      $"process_date".cast(IntegerType),
      $"id_cierre".cast(IntegerType),
      $"year_month".cast(IntegerType),
      $"provider".cast(ShortType)
    )

    newSchemaDf.write
      .mode("overwrite")
      .partitionBy(partitions: _*)
      .option("path", outputFilePath)
      .saveAsTable(outputTable)
  }

  def insertDataFrame(df: DataFrame, outputTable: String): Unit = {
    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    val columns = spark.sql(s"show columns in ${outputTable}").collect().map(_.getString(0).trim)
    val dfToInsert = df.select(columns.map(col):_*)

    println("InsertDf Schema:")
    dfToInsert.printSchema()

    dfToInsert.write
      .mode("overwrite")
      .insertInto(outputTable)

    println(s"Tabla ${outputTable} cargada")
  }

}
